package com.belenok.booklist;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.belenok.booklist.db.BookContract;
import com.belenok.booklist.db.BookDbHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private BookDbHelper mHelper;
    private ListView mBookListView;
    private SimpleAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHelper = new BookDbHelper(this);
        mBookListView = (ListView) findViewById(R.id.list_book);

        updateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_book:

                LinearLayout layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.setPadding(60,0,60,0);

                final EditText bookEditText = new EditText(this);
                final EditText authorEditText = new EditText(this);

                bookEditText.setHint("book title");
                authorEditText.setHint("book author");
                layout.addView(bookEditText);
                layout.addView(authorEditText);

                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Add a new book")
                        .setMessage("What do you want to do next?")
                        .setView(layout)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String title = String.valueOf(bookEditText.getText());
                                String author = String.valueOf(authorEditText.getText());

                                SQLiteDatabase db = mHelper.getWritableDatabase();

                                ContentValues values = new ContentValues();

                                values.put(BookContract.BookEntry.COL_BOOK_TITLE, title);
                                values.put(BookContract.BookEntry.COL_BOOK_AUTHOR, author);

                                db.insertWithOnConflict(BookContract.BookEntry.TABLE,
                                        null,
                                        values,
                                        SQLiteDatabase.CONFLICT_REPLACE);
                                db.close();
                                updateUI();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void deleteBook(String title) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.delete(BookContract.BookEntry.TABLE,
                BookContract.BookEntry.COL_BOOK_TITLE + " = ?",
                new String[]{title});
        db.close();
        updateUI();
    }


    private static class Book {
        public final String title;
        public final String author;

        public Book(String title, String author) {
            this.title = title;
            this.author = author;
        }
    }



    private void updateUI() {
        final List<Map<String, String>> data = new ArrayList<>();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(BookContract.BookEntry.TABLE,
                new String[]{
                            BookContract.BookEntry._ID,
                            BookContract.BookEntry.COL_BOOK_TITLE,
                            BookContract.BookEntry.COL_BOOK_AUTHOR},
                null, null, null, null, null);

        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(BookContract.BookEntry.COL_BOOK_TITLE);
            int one_more_index = cursor.getColumnIndex(BookContract.BookEntry.COL_BOOK_AUTHOR);
            String title = cursor.getString(idx);
            String author = cursor.getString(one_more_index);

            Map<String, String> book = new HashMap<>();
            book.put("First Line", title);
            book.put("Second Line", author);

            data.add(book);
        }

        cursor.close();
        db.close();

        mAdapter = new SimpleAdapter(this, data, android.R.layout.simple_list_item_2,
                new String[]{"First Line", "Second Line"}, new int[]{android.R.id.text1, android.R.id.text2});

        mBookListView.setAdapter(mAdapter);
        mBookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Delete book")
                        .setMessage("Ти увєрен шо хоч удалить?")
                        .setPositiveButton("DA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteBook(data.get(position).get("First Line"));
                            }
                        })
                        .setNegativeButton("NE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create()
                        .show();

            }
        });
    }
}
