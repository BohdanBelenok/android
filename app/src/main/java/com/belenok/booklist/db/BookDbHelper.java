package com.belenok.booklist.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BookDbHelper extends SQLiteOpenHelper {

    public BookDbHelper(Context context) {
        super(context, BookContract.DB_NAME, null, BookContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + BookContract.BookEntry.TABLE + " ( " +
                BookContract.BookEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                BookContract.BookEntry.COL_BOOK_TITLE + " TEXT NOT NULL, " +
                BookContract.BookEntry.COL_BOOK_AUTHOR + " TEXT NOT NULL);";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + BookContract.BookEntry.TABLE);
        onCreate(db);
    }
}
