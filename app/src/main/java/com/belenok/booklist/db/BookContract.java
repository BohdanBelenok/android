package com.belenok.booklist.db;

import android.provider.BaseColumns;

public class BookContract {
    public static final String DB_NAME = "com.belenok.booklist.db";
    public static final int DB_VERSION = 1;

    public class BookEntry implements BaseColumns {
        public static final String TABLE = "books";

        public static final String COL_BOOK_TITLE = "title";
        public static final String COL_BOOK_AUTHOR = "author";

    }
}
